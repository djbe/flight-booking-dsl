@echo off

echo Starting Adameus server

if "%1"=="" goto NORMAL
if /I "%1"=="-v" (goto VERBOSE) else goto NORMAL

:NORMAL
java -jar Adameus.jar -home > NUL
goto DONE

:VERBOSE
java -jar Adameus.jar -home
goto DONE

:DONE
echo Done!
