#!/bin/sh

echo "Starting Adameus server"
current=`pwd`
cd `dirname $0`

if [ "$1" = "-v" ]; then
	java -jar Adameus.jar -home
else
	java -jar Adameus.jar -home > /dev/null
fi
