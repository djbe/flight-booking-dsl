require_relative 'Flight'

class Trip < Flight
  attr_reader :flights
  private :airline
  private :days_of_week # doesn't seem useful to support these
  private :on_day
  
  # constructor
  def initialize(*args)
    raise 'Trip needs at least one flight' if (args.length < 1)
    
    @flights = [*args.flatten]
    if (@flights.is_a? Flight)
      @flights = [@flights]
    end
      
    @availability = {}
    @code = @flights.collect { |f| f.code }
    @departure = @flights.first.departure
    @arrival = @flights.last.arrival
    @duration = @arrival - @departure 
    @from = @flights.first.from
    @to = @flights.last.to
  end
  
  # gets the number of available seats and their price for a given Class
  def availability(flightClass)
    flightClass = FlightClass.to_sym(flightClass)
    if (!@availability[flightClass])
      fetchAvailability(flightClass)
    end
    
    return @availability[flightClass][:seats], @availability[flightClass][:price]
  end
  
  # gets the number of available seats for a given Class
  def seats(flightClass)
    flightClass = FlightClass.to_sym(flightClass)
    if (!@availability[flightClass])
      fetchAvailability(flightClass)
    end
    
    return @availability[flightClass][:seats]
  end
  
  # gets the price for a given Class
  def price(flightClass)
    flightClass = FlightClass.to_sym(flightClass)
    if (!@availability[flightClass])
      fetchAvailability(flightClass)
    end
    
    return @availability[flightClass][:price]
  end
  
  def to_s
    list = @flights.inject(self.from) { |memo, f| "#{memo} -> #{f.to}" }
    "#{list} (#{self.departure.strftime('%FT%H:%M')} - #{self.arrival.strftime('%FT%H:%M')})"
  end
  
  private
  
  # fetch num. seats and pricing info for a certain class
  def fetchAvailability(flightClass)
    s, p = [], []
    
    @flights.each { |f|
      s << f.seats(flightClass)
      p << f.price(flightClass)
    }
    
    @availability[flightClass] = {
      :seats => s.min,
      :price => p.reduce(:+)
    }
  end
end
