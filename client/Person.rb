require_relative 'util/MapExtras.rb'
require_relative 'util/Enum'

# describes the person's gender
class Gender < Enum
  def self.map() {:male => 'M', :female => 'F'} end
end

class Person
  attr_reader :first, :last, :gender
  
  # constructor
  def initialize(first, last, gender)
    @first = first
    @last = last
    @gender = Gender.to_sym(gender)
  end
  
  # full name of person
  def to_s
    "#{@first} #{@last}"
  end
end

# map hash to person, parameters:
# first: first name
# last: last name
# gender: person's gender
def person(map)
  map.check_keys([:first, :last, :gender])
  return Person.new(map[:first], map[:last], map[:gender])
end
