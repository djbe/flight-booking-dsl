require 'net/telnet'
require 'singleton'
require 'socket'
require 'timeout'

class Adameus
  include Singleton
  private_class_method :instance
  
  attr_reader :ip, :port
  
  def initialize(host = 'localhost', port = 12111)
    @ip = resolve_host_to_ip(host)
    @port = port
    
    # check server
    raise 'Adameus is not running!' if (!is_port_open?(@ip, @port))
  end
  
  # get the Adameus server version
  def self.version
    instance.query('V').strip
  end
  
  # query the Adameus server
  def self.query(message)
    host = Net::Telnet.new('Host' => instance.ip, 'Port' => instance.port)
    result = []
    
    # execute
    host.cmd(message) { |str|
      if (!str.nil?)
        str.lines { |line|
          result << line.strip
        }
      end
    }
    host.close
    
    # check for errors
    raise 'Ill formatted message' if (result[0] == 'ERRIM')
    raise 'Empty command' if (result[0] == 'ERREC')
    raise 'No Results' if (result[0] == 'ERRNR')
    raise 'Something pretty serious happened' if (result[0] == 'ERRXX')
    
    result
  end
  
  private
  
  # convert a host to it's IP
  def resolve_host_to_ip(host)
    info = Socket::getaddrinfo(host, nil, nil, Socket::SOCK_STREAM)
    
    # the resolved ip is the second item in the resulting array
    info[1][3]
  end
  
  # check if a certain port is open at a given IP
  def is_port_open?(ip, port)
    begin
      Timeout::timeout(1) do
        begin
          s = TCPSocket.new(ip, port)
          s.close

          return true
        rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH
          return false
        end
      end
    rescue Timeout::Error
    end

    return false
  end
end
