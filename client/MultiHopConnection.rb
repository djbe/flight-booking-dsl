require_relative 'Trip'
require_relative 'util/MapExtras'

class MultiHopConnection
  attr_reader :departure, :destination, :routes
  
  def initialize(departure, destination, max_hops = 3)
    @departure = departure
    @destination = destination
    @routes = []
    
    findConnectionsBreadthFirst(max_hops)
  end
  
  # list of found routes, ranked by price (given a class)
  # route: list of airports
  # fclass: flight class
  # departure: departure date
  def rankedCheapest(route, fclass, departure)
    flightList = findTripsForRoute(route.clone, [], departure)
    
    flightList.sort_by { |fn| fn.price(fclass) }
  end
  
  
  # list of found routes, ranked by price (given a class)
  # departure: departure date
  # fclass: flight class
  def rankCheapest(fclass, departure)
    if (@routes.length == 0)
        return []
    end
    
    result = rankedCheapest(@routes[0], fclass, departure)
    for i in (1..@routes.length-1)
        list = rankedCheapest(@routes[i], fclass, departure)
        result = mergeCheapest(result, list, fclass)
    end
    
    result
  end

  # list of found routes, ranked by how fast the connections are
  # route: list of airports
  # departure: departure date
  def rankedFastest(route, departure)
    flightList = findTripsForRoute(route.clone, [], departure)

    flightList.sort_by { |fn| fn.duration }
  end

  # list of found routes, ranked by travel time
  # departure: departure date
  # fclass: flight class
  def rankFastest(departure)
    if (@routes.length == 0)
        return []
    end

    result = rankedFastest(@routes[0], departure)
    for i in (1..@routes.length-1)
        list = rankedFastest(@routes[i], departure)
        result = mergeFastest(result, list)
    end

    result
  end
  
#  private
  
  #merge strategy: take cheapest of
  def mergeCheapest(fl1, fl2, fclass)
    
    nbfl1 = fl1.length
    nbfl2 = fl2.length
    
    fl3 = Array.new(nbfl1 + nbfl2)
    
    j = 0
    k = 0
    for i in (0.. fl3.length-1)
      if(j == nbfl1)
        fl3[i] = fl2[k]
        k = k+1
      elsif(k == nbfl2)
        fl3[i] = fl1[j]
        j = j+1
      elsif(fl1[j].price(fclass) < fl2[k].price(fclass) )
        fl3[i] = fl1[j]
        j = j+1
      else
        fl3[i] = fl2[k]
        k = k+1
      end
      
      i = i+1
    end
    
    fl3
  end
  
  #merge strategy: take shortest duration of
  def mergeFastest(fl1, fl2)
    
    nbfl1 = fl1.length
    nbfl2 = fl2.length
    
    fl3 = Array.new(nbfl1 + nbfl2)
    
    j = 0
    k = 0
    for i in (0.. fl3.length-1)
      if(j == nbfl1)
        fl3[i] = fl2[k]
        k = k+1
      elsif(k == nbfl2)
        fl3[i] = fl1[j]
        j = j+1
      elsif(fl1[j].duration < fl2[k].duration )
        fl3[i] = fl1[j]
        j = j+1
      else
        fl3[i] = fl2[k]
        k = k+1
      end
      
      i = i+1
    end
    
    fl3
  end
  
  # find a list of connection combinations with at most max_hops hops
  def findConnectionsBreadthFirst(max_hops)
    @queue = [[departure]]  # 2D array
    
    while !(@queue.empty?)
      currentHop = @queue.shift
      
      if (currentHop.length <= max_hops)
        currentAirport = currentHop[currentHop.size-1]
        connections = currentAirport.destinations
        
        connections.each { |connection|
          if (connection == @destination)
            route = (currentHop.clone) << connection
            @routes << route
          elsif (!currentHop.include?(connection))
            route = (currentHop.clone) << connection
            @queue << route
          end
        }
      end
    end
  end
  
  # find all possible flight connections given a route
  def findTripsForRoute(route, flights, date)
    airport = route.shift # departure
    
    # finished recursion, return found combination
    if (!route.first)
      return Trip.new(flights)
    end
    
    # find viable connections
    viable = airport.connections(route.first, date.to_date)
    viable.select { |c| c.departure >= date }
    
    # process each viable connection
    viable.collect { |flight|
      t = findTripsForRoute(route.clone, flights.clone << flight, flight.arrival)
    }.flatten
  end
end

# map hash to multiconnection
def multihop(map)
  map.check_keys([:departure, :destination, :max_hops])
  return MultiHopConnection.new(map[:departure], map[:destination], map.fetch(:max_hops, 3))
end
