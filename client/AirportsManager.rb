require 'singleton'
require_relative 'Adameus'
require_relative 'Airport'

class AirportsManager
  include Singleton
  private_class_method :instance
  
  attr_reader :airports
  
  def initialize
    @airports = Hash.new
    
    # fetch all airports
    Adameus.query('P').each { |x|
      a = Airport.new(x)
      @airports[a.code] = a
    }
  end
  
  # get an airport given an airport code
  def self.get(code)
    instance.airports[code]
  end
end
