# encoding: utf-8

# Fix for incompatibility between Ruby 1.8 and 1.9
unless Kernel.respond_to?(:require_relative)
  module Kernel
    def require_relative(path)
      require File.join(File.dirname(caller[0]), path.to_str)
    end
  end
end

require_relative 'FBFramework'

module ExampleScenario
  
  #We want to book a trip for two people from Iceland (KEF) to Sydney (SYD), and we would like the trip to be as short as possible.
  #We leave on the 16th december 2011
  testDate = Date.new(2011,12,16)
  
  #This returns the 5 fastest trips from Iceland to Sydney under the given conditions. 'Airport.KEF' is shorthand notation to find the airport with code KEF.
  #The 'seats' parameter indicates how many seats we need to book, and 'amount' => 5 means that the system will show the 5 fastest connections.
  trips = fastest(:departure => Airport.KEF, 
                  :destination => Airport.SYD, 
                  :date => testDate, 
                  :seats => 2, 
                  :amount => 5, 
                  :mode => :all)
  puts trips.size
  trips.each { |t|
    puts "- #{t}, duration: #{t.duration}"
  }
  
  #From the given trips, we choose the first (fastest) one.
  trip = trips.first
  
  #Temporarily hold a booking. Note that we choose to travel first class.
  holding = hold(:trip => trip,
                 :class => :first,
                 :person => [
                    person(:first => "Frank", :last => "Janssens", :gender => :male),
                    person(:first => "Els", :last => "Janssens", :gender => :female)
                 ])
  
  #For some reason, we want to cancel this holding, and book another flight
  holding.cancel
  
  #Create a new holding, take the second trip of the list we were given.
  trip = trips[1]
  holding = hold(:trip => trip, 
                :class => :first, 
                :person => [
                    person(:first => "Frank", :last => "Janssens", :gender => :male),
                    person(:first => "Els", :last => "Janssens", :gender => :female)])
  
  #Finalize the booking
  booking = holding.book
  
  
  #Scenario ends here, but we still clean-up and remove the booking for ease of testing
  booking.cancel
  
  
end
