require 'date'

class Rational
  def to_s
    ss, fr = self.divmod(1/86400.0)
    h, ss = ss.divmod(3600)
    min, s  = ss.divmod(60)
    
    "#{h}h #{min}m #{s}s"
  end
end
