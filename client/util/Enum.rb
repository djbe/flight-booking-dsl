
class Enum
  # convert enum value from symbol to string (and vice versa)
  def self.convert(value)
    case value
    when String
      if (map.has_value?(value))
        map.invert[value]
      else
        raise 'Incorrect enum string value'
      end
    when Symbol
      if (map.has_key?(value))
        map[value]
      else
        raise 'Incorrect enum symbol value'
      end
    else
      raise 'Incorrect enum value type'
    end
  end
  
  # convert to string representation
  def self.to_s(value)
    case value
    when String
      value
    else
      convert(value)
    end
  end
  
  # convert to symbol representation
  def self.to_sym(value)
    case value
    when Symbol
      value
    else
      convert(value)
    end
  end
  
  # dynamic reception
  def self.method_missing(value)
    convert(value)
  end
end
