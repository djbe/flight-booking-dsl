require 'date'

class DateTime
  # create instance from date object (or string) and time string
  def self.fromDateAndTime(date, time)
    self.parse(date.to_s + 'T' + time)
  end
  
  # get duration of instance (only valid for times on today date)
  def duration
    self - Date.today
  end
  
  # find the next day (with name) after this date
  def find_next_day(name)
    self + (Date::DAYNAMES.index(name) - self.wday) % 7
  end
  
  # convert to Date instance
  def to_date
    Date.new(year, month, day)
  end
  
  # convert to DateTime instance
  def to_datetime
    self
  end
end

class Date
  # convert to Date instance
  def to_date
    self
  end
  
  # convert to DateTime instance
  def to_datetime
    DateTime.new(year, month, day)
  end
end
