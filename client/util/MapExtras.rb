
class Hash
  # check if all the keys in this hash belong to a list of valid keys
  def check_keys(validKeys)
    badKeys = self.keys - validKeys
    raise IncorrectKeyException.new(badKeys) unless badKeys.empty?
  end
end
