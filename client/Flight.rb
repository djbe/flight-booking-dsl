require_relative 'AirportsManager'
require_relative 'AirlinesManager'
require_relative 'util/DateExtras'
require_relative 'util/Enum'

# describes the flight seating class
class FlightClass < Enum
  def self.map() {:first => 'F', :business => 'B', :economy => 'E'} end
end

# flight type
class Flight
  attr_reader :code, :departure, :duration, :arrival, :airline
  
  # constructor
  def initialize(*args)
    case args.size
    when 5
      init_normal(*args)
    when 4
      init_connection(*args)
    when 1
      init_booking(*args)
    else
      raise 'Incorrect number of arguments'
    end
  end
  
  # departure airport
  def from
    if (@from.nil?)
      fetchAirports
    end
    @from
  end
  
  # destination airport
  def to
    if (@to.nil?)
      fetchAirports
    end
    @to
  end
  
  # get the days of the week on which this flight is valid
  # returns a hash of day names to booleans
  def days_of_week
    result = Hash.new
    q = Adameus.query("W#{@code}").first
    raise "Unknown flight code" if (q == 'FN')
    
    for i in (0..6)
      result[Date::DAYNAMES[i]] = (q[i, 1] == "Y")
    end
    
    result
  end
  
  # get copy of flight on the next day with name 'day'
  def on_day(day)
    available = self.days_of_week
    
    if (available.has_key?(day))
      date = @departure.find_next_day(day)
      Flight.new(@code, date, @duration, @from, @to)
    else
      raise 'Unavailable week day for this flight'
    end
  end
  
  # gets the number of available seats and their price for a given Class
  def availability(flightClass)
    flightClass = FlightClass.to_sym(flightClass)
    
    if (!@availability[flightClass])
      fetchAvailability(flightClass)
    end
    
    return @availability[flightClass][:seats], @availability[flightClass][:price]
  end
  
  # gets the number of available seats for a given Class
  def seats(flightClass)
    flightClass = FlightClass.to_sym(flightClass)
    
    if (!@availability[flightClass])
      fetchAvailability(flightClass)
    end
    
    return @availability[flightClass][:seats]
  end
  
  # gets the price for a given Class
  def price(flightClass)
    flightClass = FlightClass.to_sym(flightClass)
    
    if (!@availability[flightClass])
      fetchAvailability(flightClass)
    end
    
    return @availability[flightClass][:price]
  end
  
  def to_s
    "#{@code}: #{@from} -> #{@to} (#{@departure.strftime('%F %H:%M')}-#{@arrival.strftime('%H:%M')})"
  end
  
  private
  
  # normal constructor
  def init_normal(code, date, duration, from, to)
    @code = code
    @airline = AirlinesManager.get(@code[0, 3])
    @departure = date
    @arrival = date + duration
    @duration = duration
    @from = from
    @to = to
    @availability = {}
  end
  
  # constructor for connections query
  def init_connection(text, date, from, to)
    raise 'Incorrect flight text' if (text.length != 16)
    
    @code = text[0, 6]
    @airline = AirlinesManager.get(@code[0, 3])
    @departure = DateTime.fromDateAndTime(date, text[6, 5])
    @duration = DateTime.parse(text[11, 5]).duration
    @arrival = @departure + @duration
    @from = from
    @to = to
    @availability = {}
  end
  
  # constructor for booking query
  def init_booking(text)
    raise 'Incorrect argument type' if (!text.is_a? String)
    raise 'Incorrect argument length' if (text.length != 26)
    
    @departure = DateTime.fromDateAndTime(text[0, 10], text[10, 5])
    @duration = DateTime.parse(text[15, 5]).duration
    @arrival = @departure + @duration
    @code = text[20, 6]
    @airline = AirlinesManager.get(@code[0, 3])
    @availability = {}
  end
  
  # fetch departure and arrival airports
  def fetchAirports
    q = Adameus.query("F#{@code}").first
    raise "Unknown flight code" if (q == 'FN')
    
    @from = AirportsManager.get(q[0, 3])
    @to = AirportsManager.get(q[3, 3])
  end
  
  # fetch num. seats and pricing info for a certain class
  def fetchAvailability(fclass)
    q = Adameus.query("S#{@departure.to_date}#{@code}#{FlightClass.to_s(fclass)}").first
    
    @availability[fclass] = {
      :seats => q[0, 3].to_i,
      :price => q[3, 5].to_i
    }
  end
end
