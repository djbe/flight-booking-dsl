require_relative 'AirlinesManager'

class Airline
  attr_reader :code, :name
  
  # constructor for airline query
  def initialize(text)
    raise 'Incorrect airline text' if (text.length < 3)
    
    @code = text[0, 3]
    @name = text[3, 30]
  end
  
  # pretty print
  def to_s
    "#{@name} (#{@code})"
  end
  
  # dynamic reception (airline code)
  def self.method_missing(id)
    AirlinesManager.get(id.to_s)
  end
end
