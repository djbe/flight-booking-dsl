# encoding: utf-8
#require_relative 'Trip'

class Booking
  attr_reader :code, :trip, :class, :passenger, :status, :price
  
  # constructor
  def initialize(*args)
    case args.size
    when 3
      init_holding(*args) # trip, class, person
    when 1
      init_id(*args) # ID
    else
      raise 'Incorrect number of arguments'
    end
  end


  # book a holding
  def book
    raise 'Can\'t book when not holding' if (@status != :holding)
    
    begin
      @price = @code.split('-').collect { |code|
         book_code(code)
      }.inject(:+)
      @status = :booked
    rescue RuntimeError
      @status = :cancelled
			@code.split('-').each { |code| cancel_code(code) }
			raise 'Couldn\'t book whole trip'
		end
		
		self
  end
  
  # cancel a booking or holding
  def cancel
    @status = :cancelled
		@code.split('-').each { |code| cancel_code(code) }
		
		self
  end


  def to_s
    "Booking Code: #{@code}" +
    "\nPassenger: \t  #{@passenger}" +
    "\nTrip: \t #{@trip}" +
    "\nClass: \t #{@class}" +
    "\nStatus:\t #{@status}" +
    "\nPrice: \t € #{@price} \n\n"
  end

  private
  
  # try to create a holding
  def init_holding(trip, fclass, person)
    
    # we always want trips
    if (!trip.instance_of? Trip)
      trip = Trip.new(trip)
    end
    
    # store general info
    @status = :none
    @trip = trip
    @class = fclass
    @passenger = person
    @price = -1
    
    # hold all of trip's flights
    codes = []
    begin
      trip.flights.each { |f| 
        q = Adameus.query("H#{f.departure.to_date}#{f.code}#{FlightClass.to_s(@class)}#{Gender.to_s(@passenger.gender)}%-15s%-20s" % [@passenger.first, @passenger.last]).first
        raise 'No seat available in specified category' if (q[0, 1] != 'S')
        
        codes << q[1, 32]
      }
    rescue RuntimeError => e
      puts e.message
      codes.each { |code| self.cancel_code(code) }
			raise 'Couldn\'t hold complete trip'
    end
    
    # store code
    @code = codes.join('-')
    @status = :holding
  end
  
  # load booking or holding based on ID
  def init_id(id)
    @code = id
    @price = 0
    
    # fetch flights
    flights = id.split('-').collect { |subID|
      q = Adameus.query("Q#{subID}").first
      raise "Unknown booking/holding number" if (q == 'FN')
      
      # for first flight, also load status, class and passenger
      if (@price == 0)
        @status = (q[0, 1] == 'H') ? :holding : :booked
        @class = FlightClass.to_sym(q[27, 1])
        @passenger = Person.new(q[29, 15].strip, q[44, 20].strip, q[28, 1])
      end
      
      # create flight object
      @price += q[64, 5].to_i
      Flight.new(q[1, 26])
    }
    
    # store flights into trip
    @trip = Trip.new(flights)
  end
  
  # book a holding code (returns the price)
  def book_code(code)
    q = Adameus.query("B#{code}").first
    
    if (q[0, 1] == 'S')
      q[64, 5].to_i
    else
      raise 'Invalid booking number' if (q[1, 1] == 'N')
      raise 'Seat already booked' if (q[1, 1] == 'A')  
    end
  end
  
  # cancel a booking or holding code
  def cancel_code(code)
    q = Adameus.query("X#{code}").first
    
    raise 'Invalid booking/holding number' if (q[1, 1] == 'N')
    raise 'Booking is older than 7 days, cannot be cancelled' if (q[1, 1] == 'X')
  end
end
