# encoding: utf-8

# Fix for incompatibility between Ruby 1.8 and 1.9
unless Kernel.respond_to?(:require_relative)
  module Kernel
    def require_relative(path)
      require File.join(File.dirname(caller[0]), path.to_str)
    end
  end
end

require_relative 'FBFramework'

def testDestinations
  puts
  puts "Destinations for #{Airport.LAX}:"
  
  Airport.LAX.destinations.each { |d| puts " - #{d}" }
end

def testConnections
  puts
  puts "Connections from #{Airport.LAX} to #{Airport.FRA} for today:"
  
  Airport.LAX.connections(Airport.FRA, Date.today).each { |f|
    seats, price = f.availability(:first)
    puts "#{f}, seats: #{seats}, price: €#{price}"
  }
end

def testMultiHop
  puts
  puts "Finding routes: "
  puts
  
  hops = multihop(:departure => Airport.FRA, :destination => Airport.NRT)
  hops.routes.each { |route|
    route.each { |e| print "#{e} " }
    puts ''
  }

  puts
  puts "Fastest trip for route #3: "

  hops.rankedFastest(hops.routes[2], DateTime.now).each { |t|
    puts "- #{t}, duration: #{t.duration}"
  }

  puts
  puts "Cheapest trip for route #1: "

  hopsCH3 = hops.rankedCheapest(hops.routes[0], :first, DateTime.now)
  hopsCH3.each { |t|
    puts "- #{t}, price: €#{t.price(:first)}, seats: #{t.seats(:first)}"
  }
end

def testSearchEngine
  puts
  puts "Ten fastest trips overall: (simple)"
  puts
  
  fastest(:departure => Airport.FRA, :destination => Airport.NRT,
          :date => DateTime.now).each { |t|
    puts "- #{t}, duration: #{t.duration}"
  }
  
  puts
  puts "Ten fastest trips overall: (all routes)"
  puts
  
  fastest(:departure => Airport.FRA, :destination => Airport.NRT,
          :date => DateTime.now, :mode => :all).each { |t|
    puts "- #{t}, duration: #{t.duration}"
  }
  
  puts
  puts "Ten cheapest trips overall: (simple)"
  puts 
  
  cheapest(:departure => Airport.FRA, :destination => Airport.NRT,
           :date => DateTime.now, :class => :first).each { |t|
    puts "- #{t}, duration: #{t.duration}, price: €#{t.price(:first)}"
  }
  
  puts
  puts "Ten cheapest trips overall: (all routes, might take a while)"
  puts 
  
  cheapest(:departure => Airport.FRA, :destination => Airport.NRT,
           :date => DateTime.now, :class => :first, :mode => :all).each { |t|
    puts "- #{t}, duration: #{t.duration}, price: €#{t.price(:first)}"
  }
end

def testBooking
  puts
  print 'Finding trips... '
  
	trip1 = fastest(:departure => Airport.FRA, :destination => Airport.NRT,:date => DateTime.now,:seats => 3)[0]
	trip2 = fastest(:departure => Airport.LAX, :destination => Airport.LHR,:date => DateTime.now,:seats => 4)[0]
	trip3 = fastest(:departure => Airport.PEK, :destination => Airport.BRU,:date => DateTime.now,:seats => 2)[0]
	trip4 = fastest(:departure => Airport.AKL, :destination => Airport.LAX,:date => DateTime.now,:seats => 4)[0]
	trip5 = fastest(:departure => Airport.BRU, :destination => Airport.BCN,:date => DateTime.now,:seats => 1)[0]
	trip6 = fastest(:departure => Airport.FRA, :destination => Airport.BCN,:date => DateTime.now,:seats => 4)[0]
  
  puts 'done!'
	puts
	puts "Create six holdings, book them and cancel after"
	puts
	
	p1 = person(:first => "UF1", :last => "UL1", :gender => :male) 
	p2 = person(:first => "UF2", :last => "UL2", :gender => :female) 
	p3 = person(:first => "UF3", :last => "UL3", :gender => :male) 
	p4 = person(:first => "UF4", :last => "UL4", :gender => :female) 
	p5 = person(:first => "UF5", :last => "UL5", :gender => :male) 
	p6 = person(:first => "UF6", :last => "UL6", :gender => :male) 

  puts "Holding 1\n---------"
	holding1 = hold(:trip => trip1, :class => :first, :person => [p1,p2,p3])
	holding1.book

	holding1.cancel
  puts holding1

  puts "Holding 2\n---------"
	holding2 = hold(:trip => trip2, :class => :business, :person => [p3,p4,p5,p6])
	holding2.book
	holding2.cancel
  puts holding2

  puts "Holding 3\n---------"
	holding3 = hold(:trip => trip3, :class => :first, :person => [p5,p2])
	holding3.book
	holding3.cancel
  puts holding3

  puts "Holding 4\n---------"
	holding4 = hold(:trip => trip4, :class => :business, :person => [p1,p2,p3,p4])
	holding4.book
	holding4.cancel
  puts holding4

  puts "Holding 5\n---------"
	holding5 = hold(:trip => trip5, :class => :economy, :person => p1)
	holding5.book
	holding5.cancel
  puts holding5

  puts "Holding 6\n---------"
	holding6 = hold(:trip => trip6, :class => :economy, :person => [p1,p2,p3,p4])
	holding6.book
  testBooking = hold(:id => holding6.code )
	holding6.cancel
  puts holding6
end

# ---- Testing ----

testDestinations()
ttestConnections()
testMultiHop()
testSearchEngine()
testBooking()
