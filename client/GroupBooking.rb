# encoding: utf-8
require_relative 'Booking'

class GroupBooking < Booking
	attr_reader :bookings, :passengers
  private :passenger # doesn't contain data, passengers is used instead
  
  # constructor
  def initialize(*args)
    case args.size
    when 3
      init_holding(*args) # trip, class, persons
    when 1
      init_id(*args) # ID
    else
      raise 'Incorrect number of arguments'
    end
  end
  
  # book all holdings
	def book
		begin
			@bookings.each { |b| b.book() }
      @price = @bookings.collect{|b| b.price }.inject(:+)
			@status = :booked
		rescue RuntimeError
		  @status = :cancelled
			@bookings.each { |b| b.cancel() }
			raise 'Couldn\'t book all seats'
		end
		
		self
	end
	
	# cancel all bookings or holdings
	def cancel
	  @status = :cancelled
	  @bookings.each { |b| b.cancel() }
	  
	  self
  end

  def to_s
    result = "Booking Code: #{@code}" +
    "\nPassengers:\t " +
    @passengers.collect{|i| i.to_s }.join(", ") +
    "\nTrip:\t\t #{trip}" +
    "\nClass: \t\t #{@class}" +
    "\nStatus:\t\t #{@status}" +
    "\nPrice per passenger: € #{@bookings[0].price}" +
    "\nTotal price: \t\t € #{@price}" +
    "\n\nBooking Code per passenger: "
    @bookings.each {
        |i|
        result << "\n\t #{i.passenger}: #{i.code}"
    }
    result << "\n\n"
    result
  end

  private
  
  # try to create holdings
  def init_holding(trip, fclass, persons)
    @status = :none
		@trip = trip
		@class = fclass
    @passengers = persons
		@bookings = []
		
    # first check availability
		seats, price = trip.availability(fclass)
		raise 'Not enough seats available' if (seats < persons.size)
		
		# create holdings
		begin
			persons.each { |p|
				@bookings << Booking.new(@trip, @class, p)
			}
		rescue RuntimeError
			@bookings.each{ |b| b.cancel() }
			raise 'Couldn\'t hold all seats'
		end
		
		# store code
		@code = @bookings.collect{ |b| b.code }.join('|')
		@status = :holding
	end
  
  # load booking or holding based on ID
  def init_id(id)
		@bookings = []
    id.split('|').each { |subID| @bookings << Booking.new(subID) }
    
    @code = id
    @class = @bookings.first.class
    @status = @bookings.first.status
    @passengers = @bookings.collect{ |b| b.passenger }
    @price = @bookings.collect{ |b| b.price }.inject(:+)
  end
end

# create or load a holding depending on the parameters:
# id: holding ID
# trip: holding flight or trip
# class: desired class
# person: passenger
#
# if an ID is given, loads the holding.
# Otherwise it tries to creates a holding using the given
# parameters (trip, class, person).
def hold(map)
  map.check_keys([:trip, :class, :person, :id])
  
  if (map.has_key? :id)
    if (map[:id].include? '|')
      GroupBooking.new(map[:id])
    else
      Booking.new(map[:id])
    end
  else
    if (map[:person].is_a? Array)
      GroupBooking.new(map[:trip], map[:class], map[:person])
    else
      Booking.new(map[:trip], map[:class], map[:person])
    end
  end
end

# create or load a booking depending on the parameters:
# id: booking or holding ID
# trip: holding flight or trip
# class: desired class
# person: passenger
#
# if an ID is given, loads the booking or holding and should
# this appear to be a holding, tries to book it.
# Otherwise it tries to create a booking using the given
# parameters (trip, class, person).
def book(map)
  map.check_keys([:trip, :class, :person, :id])
  
  if (map.has_key? :id)
    holding = hold(map)
    if (holding.status == :holding)
      holding.book()
    end
    holding
  else
    hold(map).book()
  end
end
