require 'singleton'
require_relative 'Adameus'
require_relative 'Airline'

class AirlinesManager
  include Singleton
  private_class_method :instance
  
  attr_reader :airlines
  
  def initialize
    @airlines = Hash.new
    
    # fetch all airlines
    Adameus.query('A').each { |x|
      a = Airline.new(x)
      @airlines[a.code] = a
    }
  end
  
  # get an airline given an airline code
  def self.get(code)
    instance.airlines[code]
  end
end
