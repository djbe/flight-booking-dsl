require_relative 'AirportsManager'
require_relative 'Flight'

class Airport
  attr_reader :code, :name, :country
  
  # constructor for airport query
  def initialize(text)
    raise 'Incorrect airport text' if (text.length < 23)
    
    @code = text[0, 3]
    @name = text[3, 20].strip
    @country = text[23, 20]
    
    # TODO: fix database
    if (@code == 'HNC')
      puts 'Fixed Honolulu code!'
      @code = 'HNL'
    end
  end
  
  # get a list of possible destinations
  def destinations
    result = []
    
    Adameus.query("D#{@code}").each { |d|
      result << AirportsManager.get(d)
    }
    
    result
  end
  
  # get a list of possible connection flights to a city on a certain date
  def connections(destination, date)
    result = []
    
    Adameus.query("C#{@code}#{destination.code}#{date}").each { |f|
      result << Flight.new(f, date, self, destination)
    }
    
    result
  end
  
  # pretty print
  def to_s
    "#{@name} (#{@code})"
  end
  
  # dynamic reception (airport codes)
  def self.method_missing(id)
    AirportsManager.get(id.to_s)
  end
end
