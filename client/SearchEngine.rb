require_relative 'MultiHopConnection'
require 'singleton'

class SearchEngine
  include Singleton
  
  def self.getFastest(departure, destination, departureDate, fClass = :first, amount = 10, nbPeople = 0)
    hops = MultiHopConnection.new(departure, destination)
    trips = hops.rankedFastest(hops.routes[0], departureDate)
    if nbPeople >=1
      trips = filterNbPeople(trips, nbPeople, fClass)
    end
    trips[0...amount]
  end
  
  def self.getFastestAll(departure, destination, departureDate,fClass = :first, amount = 10, nbPeople = 0)
    hops = MultiHopConnection.new(departure, destination)
    trips = hops.rankFastest(departureDate)
    if nbPeople >=1
      trips = filterNbPeople(trips, nbPeople, fClass)
    end
    trips[0...amount]
  end
  
  def self.getCheapest(departure, destination, departureDate, fClass, amount = 10, nbPeople = 1)
    hops = MultiHopConnection.new(departure, destination)
    trips = hops.rankedCheapest(hops.routes[0], fClass, departureDate)
    if nbPeople >=1
      trips = filterNbPeople(trips, nbPeople, fClass)
    end
    trips[0...amount]
  end
  
  def self.getCheapestAll(departure, destination, departureDate, fClass, amount = 10, nbPeople = 1)
    hops = MultiHopConnection.new(departure, destination)
    trips = hops.rankCheapest(fClass, departureDate)
    if nbPeople >=1
      trips = filterNbPeople(trips, nbPeople, fClass)
    end
    trips[0...amount]
  end
  
  def self.filterNbPeople(trips, nbPeople, fClass)
    trips.select {|x| x.seats(fClass) >= nbPeople}
  end
 
end

# map hash to cheapest search, parameters:
# departure: departure airport
# destination: destination airport
# date: desired search date
# class: desired flight class
# amount: max amount of results (optional, default: 10)
# mode: :simple or :all (optional, default: simple)
# seats: minimum number of seats (optional, default: 1)
def cheapest(map)
  map.check_keys([:departure, :destination, :date, :class, :amount, :mode, :seats])
  
  if (map.fetch(:mode, :simple) == :simple)
    SearchEngine.getCheapest(map[:departure], map[:destination], map[:date], map[:class], map.fetch(:amount, 10), map.fetch(:seats, 1))
  elsif (map[:mode] == :all)
    SearchEngine.getCheapestAll(map[:departure], map[:destination], map[:date], map[:class], map.fetch(:amount, 10), map.fetch(:seats, 1))
  else
    raise 'Incorrect mode for getCheapest'
  end
end

# map hash to cheapest search, parameters:
# departure: departure airport
# destination: destination airport
# date: desired search date
# class: desired flight class (optional, default: first)
# amount: max amount of results (optional, default: 10)
# mode: :simple or :all (optional, default: simple)
# seats: minimum number of seats (optional, default: 0)
# To improve performance class is only used when seats is greater than 0
def fastest(map)
  map.check_keys([:departure, :destination, :date, :class, :amount, :mode, :seats])
  
  if (map.fetch(:mode, :simple) == :simple)
    SearchEngine.getFastest(map[:departure], map[:destination], map[:date], map.fetch(:class, :first), map.fetch(:amount, 10), map.fetch(:seats,0))
  elsif (map[:mode] == :all)
    SearchEngine.getFastestAll(map[:departure], map[:destination], map[:date], map.fetch(:class, :first), map.fetch(:amount, 10), map.fetch(:seats,0))
  else
    raise 'Incorrect mode for getFastest'
  end
end
